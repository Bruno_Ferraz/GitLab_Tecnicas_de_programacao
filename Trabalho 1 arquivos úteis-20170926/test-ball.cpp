/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"

void loop (Simulation &b, double dt){ /// Loop interno da main, que foi externalizado
	for (int i = 0; i < 1000; i++){
		b.step(dt);
		b.display();
	}
}

int main(int argc, char** argv)
{
  Ball ball ;
  const double dt = 1.0/100 ;
  loop(ball, dt); /// Fun��o que substituiu o loop da main
  return 0 ;
}

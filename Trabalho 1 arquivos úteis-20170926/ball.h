/** file: ball.h
 ** brief: Ball class
 ** author: Andrea Vedaldi
 **/

#ifndef __ball__
#define __ball__
#include "simulation.h"
#include <utility>

class Ball : public Simulation
{
public:
  // Constructors and member functions
  Ball() ;
  Ball(double valor_x, double valor_y); /// Construtor que permite a entrada de valores para as coordenadas x e y, de acordo com o solicitado na tarefa 5 e 6

  void setCoordenadas(double coordenada_x, double coordenada_y); /// Fun��o que insere os valores das coordenadas, de acordo com o solicitado na tarefa 5 e 6

  std::pair<double, double> getCoordenadas(); /// M�todo para a atualiza��o das coordenadas, de acordo com o solicitado na tarefa 5 e 6

  void step(double dt) ;
  void display() ;

protected:
  // Data members
  // Position and velocity of the ball
  double x ;
  double y ;
  double vx ;
  double vy ;

  // Mass and size of the ball
  double m ;
  double r ;

  // Gravity acceleration
  double g ;

  // Geometry of the box containing the ball
  double xmin ;
  double xmax ;
  double ymin ;
  double ymax ;
} ;

#endif /* defined(__ball__) */

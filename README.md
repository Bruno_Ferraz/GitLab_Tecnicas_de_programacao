Bouncing Ball

Introdu��o

Este projeto tem como objetivo simular o movimento de uma bola no eixo x e y, tendo como base uma s�rie de valores iniciais. Utilizando ferramentas gr�ficas para ilustrar este movimento, facilitando assim o entendimento do usu�rio acerca do funcionamento e resultados do programa.

Requisitos

O programa foi compilado no sistema operacional Ubuntu, vers�o 15.10, para a cria��o do gr�fico de movimento da bola foi utilizado o programa Microsoft Excel 2013 e para a visualiza��o do gr�fico foi utilizado o Octave, o compilador usado foi o GCC vers�o 4.9.2, e a vers�o do C++ usada foi a 14.

Execu��o

Para executar o processo de plot utilizando o Octave, utilizando o terminal, deve-se dirigir ao diret�rio contendo os arquivos do programa e a seguinte linha deve ser inserida no terminal (sem aspas) �$ octave plot_ball.m arquivos/ball.txt�, ap�s a execu��o, ser� criado um arquivo (ball.txt) contendo a sa�da do programa. Sendo que o n�mero da direita representa a coordenada x e o n�mero da esquerda representa o a coordenada y, cada linha representando um par de coordenadas, geradas pela repeti��o do loop presente em c�digo.
A sa�da ball.txt utilizada para gerar o gr�fico presente abaixo se encontra no diret�rio do programa.

Descri��o

1- simulation.h: Cabe�alho que cont�m uma classe abstrata, com dois m�todos virtuais: step e display.

2- ball.h: Cabe�alho que cont�m a classe Ball, herdeira de simulation, declara as vari�veis para delimitar a bola e o espa�o em que est� inserida.

3- graphics.h:  Cabe�alho que inclui os elementos relacionados com o OpenGL.

4- ball.cpp: Arquivo que implementa os m�todos herdados, construtores e m�todos set/get.

5- test-ball.cpp: Arquivo que instancia um objeto da classe Ball e chama a fun��o �loop� para realizar o processo de repeti��o das fun��es �step� e �display�.

6- graphics.cpp: Arquivo que implementa os m�todos para criar a bola e o espa�o de deslocamento.

7- test-ball-graphics.cpp: Arquivo que abre um espa�o e desenha a bola.



Diagrama de classes


![alt text](Diagrama.png)

Gr�fico

![alt text](Grafico.png)